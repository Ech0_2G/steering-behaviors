class_name SteeringBehavior extends Node2D

func get_vector() -> Vector2:
	printerr("Steering Behavior not implemented, returning Vector2.ZERO")
	return Vector2.ZERO
