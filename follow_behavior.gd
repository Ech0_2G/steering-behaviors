class_name FollowBehavior extends SteeringBehavior

var target : Node2D

func _ready():
	target = owner.target

func get_vector() -> Vector2:
	if target == null:
		printerr("Follow Behavior has no target")
		return Vector2.ZERO

	return (target.global_position - global_position).normalized()
