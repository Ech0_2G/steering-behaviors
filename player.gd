class_name Player extends CharacterBody2D

@onready var steering := %Steering

@export var target : Node2D
@export_range(1.0, 500.0, 0.5) var speed := 250.0

func _physics_process(_delta : float) -> void:
	velocity = steering.get_steering_vector() * speed
	rotation = velocity.angle()
	move_and_slide()
