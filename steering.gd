class_name Steering extends Node2D

var steering_behaviors : Array[SteeringBehavior]

func _ready():
	steering_behaviors = _get_steering_behaviors()

func _get_steering_behaviors() -> Array[SteeringBehavior]:
	var behaviors : Array[SteeringBehavior] = []
	for child in get_children():
		if is_instance_of(child, SteeringBehavior):
			behaviors.append(child)

	if behaviors.size() == 0:
		printerr("This Steering node has no Behaviors")
		queue_free()
		return [] 

	return behaviors

func get_steering_vector() -> Vector2:
	var steer = Vector2.ZERO

	for behavior in steering_behaviors:
		steer += behavior.get_vector()

	return steer
